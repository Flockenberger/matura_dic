-- regbank
-- Author: Florian Wagner
-- Last Modified: 05.10.2018

library ieee;
use ieee.std_logic_1164.all;

entity regbank is
	
	port
	(
		clk_in		:	in	std_logic;
		res_n		:	in std_logic;
		ena_in		:	in std_logic;
		d_in		:	in std_logic_vector(7 downto 0);
		q_out		:	out std_logic_vector(7 downto 0)
		
	);
end;

architecture rtl of regbank is
	begin 
		dff_simple_p: process(clk_in, res_n)
		
		begin
			if(res_n = '0') then 
				q_out <= (others =>'0');
				
			elsif(clk_in 'event and clk_in = '1') then
				if(ena_in = '1') then
					q_out <= d_in;
					
				end if;
			end if;
		end process;
end rtl;