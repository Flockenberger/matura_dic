-- shiftreg
-- Author: Florian Wagner
-- Last Modified: 10.10.2018

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
--------------------------------------------
-- ENTITY OF shiftreg	
--------------------------------------------
entity shiftreg is
	
	port
	(
		clk_in		:	in	std_logic;
		res_n		:	in std_logic;
		ena_in		:	in std_logic;
		dir_in		: 	in std_logic;
		d_in		:	in std_logic;
		q_out		:	out std_logic_vector(7 downto 0);
		
		-- parallel loading
		par_in	:	in std_logic_vector(7 downto 0); --parallel in
		load_in	:	in std_logic --enable parallel load
		
	);
end;

--------------------------------------------
-- ARCHITECTURE RTL	
--------------------------------------------
architecture rtl of shiftreg is

	signal shiftreg : std_logic_vector( 7 downto 0);

	begin 
		shiftreg_p: process(clk_in, res_n)
		
		begin
			if(res_n = '0') then 
				shiftreg <= (others =>'0');
				
			elsif(clk_in 'event and clk_in = '1') then
				
				if(ena_in = '1') then
				
					-- parallel loading
					
					if(load_in = '1') then
						shiftreg <= par_in;
					else
					
						if(dir_in = '1') then
							shiftreg <= d_in & shiftreg(7 downto 1); --shift right
							
						elsif(dir_in = '0') then --just 'else' also possible
							shiftreg <= shiftreg(6 downto 0) & d_in; -- shift left
						
						end if;
					end if;
				end if;
			end if;
		end process;
		
		q_out <= shiftreg;
		
end rtl;