-- Füllstand
-- Author: Florian Wagner
-- Last Modified: 05.10.2018


library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------
-- ENTITY OF FUELLSTAND	
--------------------------------------------
entity fuellstand is 
	port
	(
			c_in : in std_logic;
			b_in : in std_logic;
			a_in : in std_logic;
			
			L0_out : out std_logic;
			L1_out : out std_logic;
			L2_out : out std_logic
	);
	
end fuellstand;

	
--------------------------------------------
-- UNBEDINGTE ARCHITECTURE
--------------------------------------------
architecture unbedingt of fuellstand is
	
	signal L0 : std_logic;
	signal L1 : std_logic;
	signal L2 : std_logic;
	
	begin
		L0 <= b_in and a_in;
		L1 <= ((not c_in) and (not b_in) and a_in) or ( a_in and b_in and c_in);
		L2 <= (b_in and (not c_in)) or (a_in and (not b_in));
		
		L0_out <= L0;
		L1_out <= L1;
		L2_out <= L2;	
		
end architecture unbedingt;
	
	
--------------------------------------------
-- BEDINGTE ARCHITECTURE
--------------------------------------------
architecture bedingt of fuellstand is

	signal sig_in : std_logic_vector(2 downto 0);
	signal sig_out : std_logic_vector(2 downto 0);
	
	begin
		sig_in <= c_in & b_in & a_in;
		sig_out <= 	"000" when sig_in = "000" else
					  "100" when sig_in = "001" else
					  "010" when sig_in = "011" else
					  "110" when sig_in = "111" else
					  "001";
		L0_out <= sig_out(0);
		L1_out <= sig_out(1);
		L2_out <= sig_out(2);
		
end architecture bedingt;

--------------------------------------------	
-- SELEKTIVE ARCHITECTURE
--------------------------------------------
architecture selektiv of fuellstand is

	signal sig_in : std_logic_vector(2 downto 0);
	signal sig_out : std_logic_vector(2 downto 0);
	
	begin
		sig_in <= c_in & b_in & a_in;
		with sig_in select
		sig_out <= "000" when "000",
					  "100" when "001",
					  "010" when "011",
					  "110" when "111",
					  "001" when others;
		L0_out <= sig_out(0);
		L1_out <= sig_out(1);
		L2_out <= sig_out(2);
		
	end architecture selektiv;

--------------------------------------------
-- CONFIGURATION
--------------------------------------------
configuration fuellstand_conf of fuellstand is
	
	for selektiv
	end for;
	
end fuellstand_conf;