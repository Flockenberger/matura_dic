// Copyright (C) 2016  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel MegaCore Function License Agreement, or other 
// applicable license agreement, including, without limitation, 
// that your use is for the sole purpose of programming logic 
// devices manufactured by Intel and sold by Intel or its 
// authorized distributors.  Please refer to the applicable 
// agreement for further details.

// VENDOR "Altera"
// PROGRAM "Quartus Prime"
// VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

// DATE "10/05/2018 17:17:20"

// 
// Device: Altera EP4CE22F17C6 Package FBGA256
// 

// 
// This Verilog file should be used for ModelSim-Altera (Verilog) only
// 

`timescale 1 ps/ 1 ps

module fuellstand (
	c_in,
	b_in,
	a_in,
	L0_out,
	L1_out,
	L2_out);
input 	c_in;
input 	b_in;
input 	a_in;
output 	L0_out;
output 	L1_out;
output 	L2_out;

// Design Ports Information
// L0_out	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// L1_out	=>  Location: PIN_G5,	 I/O Standard: 2.5 V,	 Current Strength: Default
// L2_out	=>  Location: PIN_F3,	 I/O Standard: 2.5 V,	 Current Strength: Default
// c_in	=>  Location: PIN_D1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// b_in	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
// a_in	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default


wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
assign unknown = 1'bx;

tri1 devclrn;
tri1 devpor;
tri1 devoe;
// synopsys translate_off
initial $sdf_annotate("Fuellstand_min_1200mv_0c_v_fast.sdo");
// synopsys translate_on

wire \L0_out~output_o ;
wire \L1_out~output_o ;
wire \L2_out~output_o ;
wire \b_in~input_o ;
wire \c_in~input_o ;
wire \a_in~input_o ;
wire \Mux2~0_combout ;
wire \Mux2~1_combout ;
wire \Mux0~0_combout ;


hard_block auto_generated_inst(
	.devpor(devpor),
	.devclrn(devclrn),
	.devoe(devoe));

// Location: IOOBUF_X0_Y23_N23
cycloneive_io_obuf \L0_out~output (
	.i(\Mux2~0_combout ),
	.oe(vcc),
	.seriesterminationcontrol(16'b0000000000000000),
	.devoe(devoe),
	.o(\L0_out~output_o ),
	.obar());
// synopsys translate_off
defparam \L0_out~output .bus_hold = "false";
defparam \L0_out~output .open_drain_output = "false";
// synopsys translate_on

// Location: IOOBUF_X0_Y24_N16
cycloneive_io_obuf \L1_out~output (
	.i(\Mux2~1_combout ),
	.oe(vcc),
	.seriesterminationcontrol(16'b0000000000000000),
	.devoe(devoe),
	.o(\L1_out~output_o ),
	.obar());
// synopsys translate_off
defparam \L1_out~output .bus_hold = "false";
defparam \L1_out~output .open_drain_output = "false";
// synopsys translate_on

// Location: IOOBUF_X0_Y26_N16
cycloneive_io_obuf \L2_out~output (
	.i(\Mux0~0_combout ),
	.oe(vcc),
	.seriesterminationcontrol(16'b0000000000000000),
	.devoe(devoe),
	.o(\L2_out~output_o ),
	.obar());
// synopsys translate_off
defparam \L2_out~output .bus_hold = "false";
defparam \L2_out~output .open_drain_output = "false";
// synopsys translate_on

// Location: IOIBUF_X0_Y23_N15
cycloneive_io_ibuf \b_in~input (
	.i(b_in),
	.ibar(gnd),
	.o(\b_in~input_o ));
// synopsys translate_off
defparam \b_in~input .bus_hold = "false";
defparam \b_in~input .simulate_z_as = "z";
// synopsys translate_on

// Location: IOIBUF_X0_Y25_N8
cycloneive_io_ibuf \c_in~input (
	.i(c_in),
	.ibar(gnd),
	.o(\c_in~input_o ));
// synopsys translate_off
defparam \c_in~input .bus_hold = "false";
defparam \c_in~input .simulate_z_as = "z";
// synopsys translate_on

// Location: IOIBUF_X0_Y27_N1
cycloneive_io_ibuf \a_in~input (
	.i(a_in),
	.ibar(gnd),
	.o(\a_in~input_o ));
// synopsys translate_off
defparam \a_in~input .bus_hold = "false";
defparam \a_in~input .simulate_z_as = "z";
// synopsys translate_on

// Location: LCCOMB_X2_Y26_N16
cycloneive_lcell_comb \Mux2~0 (
// Equation(s):
// \Mux2~0_combout  = (\b_in~input_o  & ((!\a_in~input_o ))) # (!\b_in~input_o  & (\c_in~input_o ))

	.dataa(\b_in~input_o ),
	.datab(gnd),
	.datac(\c_in~input_o ),
	.datad(\a_in~input_o ),
	.cin(gnd),
	.combout(\Mux2~0_combout ),
	.cout());
// synopsys translate_off
defparam \Mux2~0 .lut_mask = 16'h50FA;
defparam \Mux2~0 .sum_lutc_input = "datac";
// synopsys translate_on

// Location: LCCOMB_X2_Y26_N18
cycloneive_lcell_comb \Mux2~1 (
// Equation(s):
// \Mux2~1_combout  = (\b_in~input_o  & \a_in~input_o )

	.dataa(gnd),
	.datab(gnd),
	.datac(\b_in~input_o ),
	.datad(\a_in~input_o ),
	.cin(gnd),
	.combout(\Mux2~1_combout ),
	.cout());
// synopsys translate_off
defparam \Mux2~1 .lut_mask = 16'hF000;
defparam \Mux2~1 .sum_lutc_input = "datac";
// synopsys translate_on

// Location: LCCOMB_X2_Y26_N28
cycloneive_lcell_comb \Mux0~0 (
// Equation(s):
// \Mux0~0_combout  = (\a_in~input_o  & (\b_in~input_o  $ (!\c_in~input_o )))

	.dataa(\b_in~input_o ),
	.datab(gnd),
	.datac(\c_in~input_o ),
	.datad(\a_in~input_o ),
	.cin(gnd),
	.combout(\Mux0~0_combout ),
	.cout());
// synopsys translate_off
defparam \Mux0~0 .lut_mask = 16'hA500;
defparam \Mux0~0 .sum_lutc_input = "datac";
// synopsys translate_on

assign L0_out = \L0_out~output_o ;

assign L1_out = \L1_out~output_o ;

assign L2_out = \L2_out~output_o ;

endmodule

module hard_block (

	devpor,
	devclrn,
	devoe);

// Design Ports Information
// ~ALTERA_ASDO_DATA1~	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_DCLK~	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_DATA0~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_nCEO~	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: 8mA

input 	devpor;
input 	devclrn;
input 	devoe;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
assign unknown = 1'bx;

wire \~ALTERA_ASDO_DATA1~~padout ;
wire \~ALTERA_FLASH_nCE_nCSO~~padout ;
wire \~ALTERA_DATA0~~padout ;
wire \~ALTERA_ASDO_DATA1~~ibuf_o ;
wire \~ALTERA_FLASH_nCE_nCSO~~ibuf_o ;
wire \~ALTERA_DATA0~~ibuf_o ;


endmodule
