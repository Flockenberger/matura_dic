-- state_machine
-- Author: Florian Wagner
-- Last Modified: 24.10.2018

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity state_machine is
	port 
	(
	  clk_i         : in  std_logic;
	  res_n         : in  std_logic;
	  enable_i      : in  std_logic;
	  output_o      : out std_logic_vector(2 downto 0)
	  
	);
	  
end entity;


architecture rtl of state_machine is  

	-- signal decleration

	type state_def is (init_state, state1, state2, state3, state4); 
	  
	attribute enum_coding		: string;
	attribute enum_coding		of state_def	:	type is "000 001 011 010 110"; -- Gray code
	
	--attribute enum_coding of state_def: type is "0000 0001 0010 0100 1000"; -- one hot

	signal state, next_state : state_def;


	begin --rtl
	  

		-- implementation of the state register
		state_reg : process (res_n, clk_i) 
		begin

			if (res_n = '0') then
				 state <= init_state;
				 
			elsif (clk_i'event and clk_i = '1') then
			  if enable_i = '1' then
				 state <= next_state;
				 
			  end if;
			end if;

		end process;

		-- implementation of the state_machine function
		state_func : process(state)
		begin

		  case state is
				  
				  when init_state => 
						next_state <= state1;
						output_o <= "000";
						  
				  when state1 => 
						next_state <= state2;
						output_o <= "001";
						
				  when state2 => 
						next_state <= state3;
						output_o <= "010";
						
				  when state3 => 
						next_state <= state4;
						output_o <= "011";
										
				  when state4 => 
						next_state <= init_state;
						output_o <= "100";
						
				  when others => -- in case of any other signal like U,X,ect...
						next_state <= init_state;
						output_o <= "000";
						
			 end case;
			 
		end process; -- state function

end rtl; -- architecture

configuration state_machine_conf of state_machine is 
  for rtl
  end for;
  
end state_machine_conf;