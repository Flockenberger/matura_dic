-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "11/09/2018 15:46:23"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ampel_steuerung IS
    PORT (
	clk_i : IN std_logic;
	res_n : IN std_logic;
	enable_i : IN std_logic;
	ampel_o : BUFFER std_logic_vector(2 DOWNTO 0)
	);
END ampel_steuerung;

-- Design Ports Information
-- ampel_o[0]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ampel_o[1]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ampel_o[2]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk_i	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- enable_i	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ampel_steuerung IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk_i : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_enable_i : std_logic;
SIGNAL ww_ampel_o : std_logic_vector(2 DOWNTO 0);
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk_i~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ampel_o[0]~output_o\ : std_logic;
SIGNAL \ampel_o[1]~output_o\ : std_logic;
SIGNAL \ampel_o[2]~output_o\ : std_logic;
SIGNAL \clk_i~input_o\ : std_logic;
SIGNAL \clk_i~inputclkctrl_outclk\ : std_logic;
SIGNAL \state.att~feeder_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \enable_i~input_o\ : std_logic;
SIGNAL \state.att~q\ : std_logic;
SIGNAL \state.stop~0_combout\ : std_logic;
SIGNAL \state.stop~q\ : std_logic;
SIGNAL \state.ready~0_combout\ : std_logic;
SIGNAL \state.ready~q\ : std_logic;
SIGNAL \state.go~feeder_combout\ : std_logic;
SIGNAL \state.go~q\ : std_logic;
SIGNAL \ampel_o~0_combout\ : std_logic;
SIGNAL \ampel_o~1_combout\ : std_logic;
SIGNAL \ALT_INV_ampel_o~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk_i <= clk_i;
ww_res_n <= res_n;
ww_enable_i <= enable_i;
ampel_o <= ww_ampel_o;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk_i~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk_i~input_o\);
\ALT_INV_ampel_o~0_combout\ <= NOT \ampel_o~0_combout\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X0_Y11_N2
\ampel_o[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \state.go~q\,
	devoe => ww_devoe,
	o => \ampel_o[0]~output_o\);

-- Location: IOOBUF_X0_Y11_N9
\ampel_o[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_ampel_o~0_combout\,
	devoe => ww_devoe,
	o => \ampel_o[1]~output_o\);

-- Location: IOOBUF_X0_Y10_N23
\ampel_o[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \ampel_o~1_combout\,
	devoe => ww_devoe,
	o => \ampel_o[2]~output_o\);

-- Location: IOIBUF_X0_Y16_N8
\clk_i~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk_i,
	o => \clk_i~input_o\);

-- Location: CLKCTRL_G2
\clk_i~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk_i~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk_i~inputclkctrl_outclk\);

-- Location: LCCOMB_X1_Y11_N2
\state.att~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \state.att~feeder_combout\ = \state.go~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \state.go~q\,
	combout => \state.att~feeder_combout\);

-- Location: IOIBUF_X0_Y16_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: IOIBUF_X0_Y12_N1
\enable_i~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_enable_i,
	o => \enable_i~input_o\);

-- Location: FF_X1_Y11_N3
\state.att\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputclkctrl_outclk\,
	d => \state.att~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \enable_i~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.att~q\);

-- Location: LCCOMB_X1_Y11_N18
\state.stop~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \state.stop~0_combout\ = !\state.att~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \state.att~q\,
	combout => \state.stop~0_combout\);

-- Location: FF_X1_Y11_N19
\state.stop\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputclkctrl_outclk\,
	d => \state.stop~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \enable_i~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.stop~q\);

-- Location: LCCOMB_X1_Y11_N10
\state.ready~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \state.ready~0_combout\ = !\state.stop~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \state.stop~q\,
	combout => \state.ready~0_combout\);

-- Location: FF_X1_Y11_N11
\state.ready\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputclkctrl_outclk\,
	d => \state.ready~0_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \enable_i~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.ready~q\);

-- Location: LCCOMB_X1_Y11_N12
\state.go~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \state.go~feeder_combout\ = \state.ready~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \state.ready~q\,
	combout => \state.go~feeder_combout\);

-- Location: FF_X1_Y11_N13
\state.go\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputclkctrl_outclk\,
	d => \state.go~feeder_combout\,
	clrn => \res_n~inputclkctrl_outclk\,
	ena => \enable_i~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \state.go~q\);

-- Location: LCCOMB_X1_Y11_N16
\ampel_o~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ampel_o~0_combout\ = (\state.go~q\) # (!\state.stop~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \state.go~q\,
	datad => \state.stop~q\,
	combout => \ampel_o~0_combout\);

-- Location: LCCOMB_X1_Y11_N20
\ampel_o~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \ampel_o~1_combout\ = (\state.ready~q\) # (!\state.stop~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \state.ready~q\,
	datad => \state.stop~q\,
	combout => \ampel_o~1_combout\);

ww_ampel_o(0) <= \ampel_o[0]~output_o\;

ww_ampel_o(1) <= \ampel_o[1]~output_o\;

ww_ampel_o(2) <= \ampel_o[2]~output_o\;
END structure;


