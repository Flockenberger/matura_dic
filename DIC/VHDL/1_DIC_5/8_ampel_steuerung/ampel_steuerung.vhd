-- ampel_steuerung
-- Author: Florian Wagner
-- Last Modified: 7.11.2018

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity ampel_steuerung is
	port 
	(
	  clk_i         : in  std_logic;
	  res_n         : in  std_logic;
	  enable_i      : in  std_logic;
	  ampel_o       : out std_logic_vector(2 downto 0)
	  
	);
	  
end entity;


architecture rtl of ampel_steuerung is  

	-- signal decleration

	type state_def is (stop, ready, go, att); 
	  
	attribute enum_coding		: string;
	attribute enum_coding		of state_def	:	type is "100 110 001 010";

	signal state, next_state : state_def;


	begin --rtl
	  

		-- implementation of the state register
		state_reg : process (res_n, clk_i) 
		begin

			if (res_n = '0') then
				 state <= stop;
				 
			elsif (clk_i'event and clk_i = '1') then
			  if enable_i = '1' then
				 state <= next_state;
				 
			  end if;
			end if;

		end process;

		-- implementation of the ampel_steuerung function
		state_func : process(state)
		begin

		  case state is
				  
				 when stop => 
						next_state <= ready;
						ampel_o <= "100";
						  
				  when ready => 
						next_state <= go;
						ampel_o <= "110";
						
				  when go => 
						next_state <= att;
						ampel_o <= "001";
						
				  when att => 
						next_state <= stop;
						ampel_o <= "010";
										
										
				  when others => -- in case of any other signal like U,X,ect...
						next_state <= stop;
						ampel_o <= "100";
						
			 end case;
			 
		end process; -- state function

end rtl; -- architecture

configuration ampel_steuerung_conf of ampel_steuerung is 
  for rtl
  end for;
  
end ampel_steuerung_conf;