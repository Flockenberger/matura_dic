-- logic_element
-- Author: Florian Wagner
-- Last Modified: 19.10.2018


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.all;

entity ampel_steuerung_tb is
end ampel_steuerung_tb;

architecture beh of ampel_steuerung_tb is

	component ampel_steuerung
	port
	(
	  clk_i         : in  std_logic;
	  res_n         : in  std_logic;
	  enable_i      : in  std_logic;
	  ampel_o      : out std_logic_vector(2 downto 0)
	);
	end component;
	
	signal clk_sig			:	std_logic;
	signal res_sig			:	std_logic;
	signal enable_sig		:	std_logic;
	signal ampel_sig		:	std_logic_vector(2 downto 0);
	signal clk_period		: 	time := 1us;
		
	begin
		dut: ampel_steuerung
		port map
		(
			clk_i => clk_sig,
			res_n => res_sig,
			enable_i => enable_sig,
			ampel_o => ampel_sig
		);
		
		-- clock generation
		clk_p: process
		begin 
			clk_sig <= '0';
			wait for 0.5 * clk_period;
			clk_sig <= '1';
			wait for 0.5 * clk_period;
		end process;
		
		--reset generation
		res_p: process
		begin
			res_sig <= '1';
			wait for 0.25 * clk_period;
			res_sig <= '0';
			wait for 1.5 * clk_period;
			res_sig <= '1';
			wait;
		end process;
		
		--enable generation
		ena_p: process
		begin
			enable_sig <= '0';
			wait for 3 * clk_period;
			enable_sig <= '1';
			wait for 20 * clk_period;
			assert false report "simulation ended!" severity failure;
		end process;
		
end architecture;