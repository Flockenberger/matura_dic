-- logic_element
-- Author: Florian Wagner
-- Last Modified: 19.10.2018


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.all;

entity logic_element_tb is
end logic_element_tb;

architecture beh of logic_element_tb is

	component logic_element
	port
	(
		clk_in			:	in		std_logic;
		res_n			:	in 	std_logic;
		ena_in			:	in 	std_logic;
		dena_in			:	in 	std_logic;
		l_in			:	in 	std_logic_vector (1 downto 0);
		l2_in			:	in 	std_logic_vector (1 downto 0);
		logic_out		:	out 	std_logic
	);
	end component;
	
	signal clk_sig			:	std_logic;
	signal res_sig			:	std_logic;
	signal ena_sig			:	std_logic;
	signal logic_sig		: 	std_logic;
	signal l_sig			: 	std_logic_vector(1 downto 0);
	signal l2_sig			:	std_logic_vector(1 downto 0);
	signal clk_period		: 	time := 1us;
	signal dena_sig			:	std_logic;
		
	begin
		uut: logic_element
		port map
		(
			clk_in  => clk_sig,
			res_n	=>	res_sig,
			ena_in	=>	ena_sig,
			dena_in => dena_sig,
			logic_out => logic_sig,
			l_in => l_sig,
			l2_in => l2_sig
			
		);
		
		l2_sig <= "11";
		dena_sig <= '1';
		
		clk_p: process
		begin 
			clk_sig <= '1';
			wait for 0.5 * clk_period;
			clk_sig <= '0';
			wait for 0.5 * clk_period;
		end process;
		
		res_p: process
		begin
			
			res_sig <= '1';
			wait for 4.25 * clk_period;
			res_sig <= '0';
			wait for 1.0 * clk_period;
			res_sig <= '1';
			wait;
		end process;
		
		ena_p: process
			begin
				ena_sig <= '0';
				wait for 3 * clk_period;
				ena_sig <= '1';
				wait;
		end process;
		
		main_p: process
			begin
				l_sig <= "00";
				wait for 2.0 * clk_period;
				l_sig <= "01";
				wait for 2.0 * clk_period;
				l_sig <= "10";
				wait for 2.0 * clk_period;
				l_sig <= "11";
				wait;
				
		end process;
end architecture;