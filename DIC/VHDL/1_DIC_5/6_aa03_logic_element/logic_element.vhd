-- logic_element
-- Author: Florian Wagner
-- Last Modified: 19.10.2018


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity logic_element is

	port
	(
		clk_in			:	in		std_logic;
		res_n			:	in 	std_logic;
		ena_in			:	in 	std_logic;
		dena_in			:	in 	std_logic;
		l_in			:	in 	std_logic_vector (1 downto 0);
		l2_in			:	in 	std_logic_vector (1 downto 0);
		logic_out		:	out 	std_logic
	);
	
end logic_element;

architecture rtl of logic_element is

	signal lut_out	:	std_logic;
	signal d_in		:	std_logic;
	signal q_out	:	std_logic;
	begin
		
		with l_in select
		lut_out <= (l2_in(0) and 	l2_in(1)) 	when "00",
					  (l2_in(0) nand 	l2_in(1)) 	when "01",
					  (l2_in(0) or 	l2_in(1)) 	when "10",
					  (l2_in(0) nor 	l2_in(1)) 	when "11",
					  (l2_in(0) and 	l2_in(1)) 	when others;
		
		d_in <= lut_out;
				  
		dff_simple_p: process(clk_in, res_n)
			begin
				if(res_n = '0') then 
					q_out <= '0';
						
				elsif(clk_in 'event and clk_in = '1') then
					if(dena_in = '1') then
						q_out <= d_in;
					end if;
				end if;
			end process;
		
		with ena_in select
		logic_out <= q_out when	'1', lut_out when others;
		
		--mux_p: process(ena_in, lut_out, q_out)
		--begin
		--	if(ena_in = '1') then 
		--		logic_out <= q_out;
		--	else
		--		logic_out <= lut_out;
		--		
		--	end if;
		--end process;
		
	
		
end architecture;