-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "10/19/2018 16:54:56"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	logic_element IS
    PORT (
	clk_in : IN std_logic;
	res_n : IN std_logic;
	ena_in : IN std_logic;
	dena_in : IN std_logic;
	l_in : IN std_logic_vector(1 DOWNTO 0);
	l2_in : IN std_logic_vector(1 DOWNTO 0);
	logic_out : BUFFER std_logic
	);
END logic_element;

-- Design Ports Information
-- logic_out	=>  Location: PIN_N3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- l2_in[0]	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- l2_in[1]	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- l_in[1]	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- l_in[0]	=>  Location: PIN_R3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ena_in	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- dena_in	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk_in	=>  Location: PIN_P2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF logic_element IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk_in : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_ena_in : std_logic;
SIGNAL ww_dena_in : std_logic;
SIGNAL ww_l_in : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_l2_in : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_logic_out : std_logic;
SIGNAL \logic_out~output_o\ : std_logic;
SIGNAL \clk_in~input_o\ : std_logic;
SIGNAL \dena_in~input_o\ : std_logic;
SIGNAL \l_in[0]~input_o\ : std_logic;
SIGNAL \l2_in[1]~input_o\ : std_logic;
SIGNAL \l_in[1]~input_o\ : std_logic;
SIGNAL \l2_in[0]~input_o\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \q_out~0_combout\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \q_out~q\ : std_logic;
SIGNAL \ena_in~input_o\ : std_logic;
SIGNAL \logic_out~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk_in <= clk_in;
ww_res_n <= res_n;
ww_ena_in <= ena_in;
ww_dena_in <= dena_in;
ww_l_in <= l_in;
ww_l2_in <= l2_in;
logic_out <= ww_logic_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X1_Y0_N23
\logic_out~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \logic_out~0_combout\,
	devoe => ww_devoe,
	o => \logic_out~output_o\);

-- Location: IOIBUF_X0_Y4_N15
\clk_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk_in,
	o => \clk_in~input_o\);

-- Location: IOIBUF_X1_Y0_N1
\dena_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_dena_in,
	o => \dena_in~input_o\);

-- Location: IOIBUF_X1_Y0_N8
\l_in[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_l_in(0),
	o => \l_in[0]~input_o\);

-- Location: IOIBUF_X0_Y5_N22
\l2_in[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_l2_in(1),
	o => \l2_in[1]~input_o\);

-- Location: IOIBUF_X0_Y4_N22
\l_in[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_l_in(1),
	o => \l_in[1]~input_o\);

-- Location: IOIBUF_X1_Y0_N15
\l2_in[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_l2_in(0),
	o => \l2_in[0]~input_o\);

-- Location: LCCOMB_X1_Y4_N18
\Mux0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = \l_in[0]~input_o\ $ (((\l2_in[1]~input_o\ & ((\l_in[1]~input_o\) # (\l2_in[0]~input_o\))) # (!\l2_in[1]~input_o\ & (\l_in[1]~input_o\ & \l2_in[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011001101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \l_in[0]~input_o\,
	datab => \l2_in[1]~input_o\,
	datac => \l_in[1]~input_o\,
	datad => \l2_in[0]~input_o\,
	combout => \Mux0~0_combout\);

-- Location: LCCOMB_X1_Y4_N28
\q_out~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \q_out~0_combout\ = (\dena_in~input_o\ & ((\Mux0~0_combout\))) # (!\dena_in~input_o\ & (\q_out~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dena_in~input_o\,
	datac => \q_out~q\,
	datad => \Mux0~0_combout\,
	combout => \q_out~0_combout\);

-- Location: IOIBUF_X0_Y6_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: FF_X1_Y4_N29
q_out : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~input_o\,
	d => \q_out~0_combout\,
	clrn => \res_n~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \q_out~q\);

-- Location: IOIBUF_X0_Y7_N1
\ena_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ena_in,
	o => \ena_in~input_o\);

-- Location: LCCOMB_X1_Y4_N20
\logic_out~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \logic_out~0_combout\ = (\ena_in~input_o\ & (\q_out~q\)) # (!\ena_in~input_o\ & ((\Mux0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \q_out~q\,
	datac => \ena_in~input_o\,
	datad => \Mux0~0_combout\,
	combout => \logic_out~0_combout\);

ww_logic_out <= \logic_out~output_o\;
END structure;


