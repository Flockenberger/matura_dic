-- zustands_folge_erkennung
-- Author: Florian Wagner
-- Last Modified: 21.11.2018


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.all;

entity zustands_folge_erkennung_tb is
end zustands_folge_erkennung_tb; 



architecture beh of zustands_folge_erkennung_tb is

	component zustands_folge_erkennung
	port
	(
	  input_i		 : in  std_logic_vector(1 downto 0);
	  clk_i          : in  std_logic;
	  res_n          : in  std_logic;
	  ena_i      	 : in  std_logic;
	  output_o		 : out std_logic
	);
	end component;
	
	signal clk_sig			:	std_logic;
	signal res_sig			:	std_logic;
	signal enable_sig		:	std_logic;
	signal out_sig			:	std_logic;
	signal clk_period		: 	time := 1us;
	signal input_sig		:  std_logic_vector(1 downto 0);
	
	begin
		dut: zustands_folge_erkennung
		port map
		(
			clk_i => clk_sig,
			res_n => res_sig,
			ena_i => enable_sig,
			output_o => out_sig,
			input_i => input_sig
		);
		
		-- clock generation
		clk_p: process
		begin 
			clk_sig <= '0';
			wait for 0.5 * clk_period;
			clk_sig <= '1';
			wait for 0.5 * clk_period;
		end process;
		
		--reset generation
		res_p: process
		begin
			res_sig <= '1';
			wait for 0.25 * clk_period;
			res_sig <= '0';
			wait for 1.5 * clk_period;
			res_sig <= '1';
			wait;
		end process;
		
		--enable generation
		ena_p: process
		begin
			enable_sig <= '0';
			wait for 0.5 * clk_period;
			enable_sig <= '1';
			wait;
			
		end process;
		
		input_p: process
		begin 
		
			input_sig <= "00";
			wait for 1.25 * clk_period;
			input_sig <= "01";
			wait for 1.25 * clk_period;
			input_sig <= "10";
			wait for 1.25 * clk_period;
			input_sig <= "01";
			wait for 1.25 * clk_period;
			
			input_sig <= "11";
			wait for 1.25 * clk_period;
			input_sig <= "10";
			wait for 1.25 * clk_period;
			input_sig <= "01";
			wait for 1.25 * clk_period;
			input_sig <= "01";
			wait for 1.25 * clk_period;
			
			input_sig <= "11";
			wait for 1.25 * clk_period;
			input_sig <= "10";
			wait for 1.25 * clk_period;
			input_sig <= "11";
			wait for 1.25 * clk_period;
			input_sig <= "01";
			wait for 1.25 * clk_period;
			wait;
			
		end process;	
			
end architecture;