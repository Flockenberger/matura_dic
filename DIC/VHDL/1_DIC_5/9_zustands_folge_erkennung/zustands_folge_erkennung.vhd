-- zustands_folge_erkennung
-- Author: Florian Wagner
-- Last Modified: 21.11.2018

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity zustands_folge_erkennung is
	port 
	(
	  input_i		 : in  std_logic_vector(1 downto 0);
	  clk_i          : in  std_logic;
	  res_n          : in  std_logic;
	  ena_i      	 : in  std_logic;
	  output_o		 : out std_logic
	  
	);
	  
end entity;


architecture rtl of zustands_folge_erkennung is  

	-- signal decleration

	type state_def is (z0, z1, z2, z3); 
	  
	attribute enum_coding		: string;
	attribute enum_coding		of state_def	:	type is "00 01 11 10";

	signal state, next_state : state_def;
	signal input : std_logic_vector(1 downto 0);

	begin --rtl
		-- implementation of the state register
		
		input <= input_i;
		
		state_reg : process (res_n, clk_i) 
		begin

			if (res_n = '0') then
					state <= z0;
				 
			elsif (clk_i'event and clk_i = '1') then
				if ena_i = '1' then
					state <= next_state;
				 
				end if;
			end if;

		end process;

		-- implementation of the zustands_folge_erkennung function
		state_func : process(state, input)
		begin

			case state is
				  
					when z0 => 
						if( input = "01") then
							next_state <= z1;
						else next_state <= z0;
						end if;
						
						output_o <= '0';
						  
					when z1 => 
						if(input = "01" ) then
							next_state <= z1;
							
						elsif(input = "11") then
							next_state <= z2;
						
						else
							next_state <= z0;
						end if;
						
						output_o <= '0';
						
					when z2 => 
						if(input = "10") then
							next_state <= z3;
							
						elsif(input = "01") then
							next_state <= z1;
							
						else
							next_state <= z0;
						end if;
						
						output_o <= '0';
						
					when z3 => 
						if(input = "01") then
							next_state <= z1;
						else
							next_state <= z0;
						end if;
						
						 output_o <= '1';			
										
					when others => -- in case of any other signal like U,X,ect...
						next_state <= z0;
						output_o <= '0';
						
			end case;
			 
		end process; -- state function

end rtl; -- architecture

configuration zustands_folge_erkennung_conf of zustands_folge_erkennung is 
  for rtl
  end for;
  
end zustands_folge_erkennung_conf;