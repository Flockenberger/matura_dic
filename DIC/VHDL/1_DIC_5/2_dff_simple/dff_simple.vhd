-- dff_simple
-- Author: Florian Wagner
-- Last Modified: 05.10.2018


library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------
-- ENTITY OF DFF_SIMPLE	
--------------------------------------------
entity dff_simple is

	port
	(
		clk_in	:	in	std_logic;
		res_n		:	in std_logic;
		ena_in	:	in std_logic;
		d_in		:	in std_logic;
		q_out		:	out	std_logic
	);
	
end entity dff_simple;


--------------------------------------------
-- ARCHITECTURE RTL	
--------------------------------------------
architecture rtl of dff_simple is
	begin 
		dff_simple_p: process(clk_in, res_n)
		
		begin
			if(res_n = '0') then 
				q_out <= '0';
				
			elsif(clk_in 'event and clk_in = '1') then
				if(ena_in = '1') then
					q_out <= d_in;
					
				end if;
			end if;
		end process;
end rtl;
