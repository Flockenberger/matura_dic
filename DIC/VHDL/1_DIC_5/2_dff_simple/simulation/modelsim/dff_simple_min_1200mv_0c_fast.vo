// Copyright (C) 2016  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel MegaCore Function License Agreement, or other 
// applicable license agreement, including, without limitation, 
// that your use is for the sole purpose of programming logic 
// devices manufactured by Intel and sold by Intel or its 
// authorized distributors.  Please refer to the applicable 
// agreement for further details.

// VENDOR "Altera"
// PROGRAM "Quartus Prime"
// VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

// DATE "10/05/2018 17:28:08"

// 
// Device: Altera EP4CE22F17C6 Package FBGA256
// 

// 
// This Verilog file should be used for ModelSim-Altera (Verilog) only
// 

`timescale 1 ps/ 1 ps

module dff_simple (
	clk_in,
	res_n,
	ena_in,
	d_in,
	q_out);
input 	clk_in;
input 	res_n;
input 	ena_in;
input 	d_in;
output 	q_out;

// Design Ports Information
// q_out	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// d_in	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ena_in	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// clk_in	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// res_n	=>  Location: PIN_P2,	 I/O Standard: 2.5 V,	 Current Strength: Default


wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
assign unknown = 1'bx;

tri1 devclrn;
tri1 devpor;
tri1 devoe;
// synopsys translate_off
initial $sdf_annotate("dff_simple_min_1200mv_0c_v_fast.sdo");
// synopsys translate_on

wire \q_out~output_o ;
wire \clk_in~input_o ;
wire \d_in~input_o ;
wire \ena_in~input_o ;
wire \q_out~0_combout ;
wire \res_n~input_o ;
wire \q_out~reg0_q ;


hard_block auto_generated_inst(
	.devpor(devpor),
	.devclrn(devclrn),
	.devoe(devoe));

// Location: IOOBUF_X0_Y7_N2
cycloneive_io_obuf \q_out~output (
	.i(\q_out~reg0_q ),
	.oe(vcc),
	.seriesterminationcontrol(16'b0000000000000000),
	.devoe(devoe),
	.o(\q_out~output_o ),
	.obar());
// synopsys translate_off
defparam \q_out~output .bus_hold = "false";
defparam \q_out~output .open_drain_output = "false";
// synopsys translate_on

// Location: IOIBUF_X0_Y5_N22
cycloneive_io_ibuf \clk_in~input (
	.i(clk_in),
	.ibar(gnd),
	.o(\clk_in~input_o ));
// synopsys translate_off
defparam \clk_in~input .bus_hold = "false";
defparam \clk_in~input .simulate_z_as = "z";
// synopsys translate_on

// Location: IOIBUF_X0_Y6_N15
cycloneive_io_ibuf \d_in~input (
	.i(d_in),
	.ibar(gnd),
	.o(\d_in~input_o ));
// synopsys translate_off
defparam \d_in~input .bus_hold = "false";
defparam \d_in~input .simulate_z_as = "z";
// synopsys translate_on

// Location: IOIBUF_X0_Y4_N22
cycloneive_io_ibuf \ena_in~input (
	.i(ena_in),
	.ibar(gnd),
	.o(\ena_in~input_o ));
// synopsys translate_off
defparam \ena_in~input .bus_hold = "false";
defparam \ena_in~input .simulate_z_as = "z";
// synopsys translate_on

// Location: LCCOMB_X1_Y5_N0
cycloneive_lcell_comb \q_out~0 (
// Equation(s):
// \q_out~0_combout  = (\ena_in~input_o  & (\d_in~input_o )) # (!\ena_in~input_o  & ((\q_out~reg0_q )))

	.dataa(\d_in~input_o ),
	.datab(gnd),
	.datac(\q_out~reg0_q ),
	.datad(\ena_in~input_o ),
	.cin(gnd),
	.combout(\q_out~0_combout ),
	.cout());
// synopsys translate_off
defparam \q_out~0 .lut_mask = 16'hAAF0;
defparam \q_out~0 .sum_lutc_input = "datac";
// synopsys translate_on

// Location: IOIBUF_X0_Y4_N15
cycloneive_io_ibuf \res_n~input (
	.i(res_n),
	.ibar(gnd),
	.o(\res_n~input_o ));
// synopsys translate_off
defparam \res_n~input .bus_hold = "false";
defparam \res_n~input .simulate_z_as = "z";
// synopsys translate_on

// Location: FF_X1_Y5_N1
dffeas \q_out~reg0 (
	.clk(\clk_in~input_o ),
	.d(\q_out~0_combout ),
	.asdata(vcc),
	.clrn(\res_n~input_o ),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.devclrn(devclrn),
	.devpor(devpor),
	.q(\q_out~reg0_q ),
	.prn(vcc));
// synopsys translate_off
defparam \q_out~reg0 .is_wysiwyg = "true";
defparam \q_out~reg0 .power_up = "low";
// synopsys translate_on

assign q_out = \q_out~output_o ;

endmodule

module hard_block (

	devpor,
	devclrn,
	devoe);

// Design Ports Information
// ~ALTERA_ASDO_DATA1~	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_DCLK~	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_DATA0~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
// ~ALTERA_nCEO~	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: 8mA

input 	devpor;
input 	devclrn;
input 	devoe;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
assign unknown = 1'bx;

wire \~ALTERA_ASDO_DATA1~~padout ;
wire \~ALTERA_FLASH_nCE_nCSO~~padout ;
wire \~ALTERA_DATA0~~padout ;
wire \~ALTERA_ASDO_DATA1~~ibuf_o ;
wire \~ALTERA_FLASH_nCE_nCSO~~ibuf_o ;
wire \~ALTERA_DATA0~~ibuf_o ;


endmodule
