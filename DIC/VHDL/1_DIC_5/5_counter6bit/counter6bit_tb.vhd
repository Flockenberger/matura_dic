LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.all;

entity counter6bit_tb is
end counter6bit_tb;

architecture beh of counter6bit_tb is

	component counter6bit
	port
	(
		clk_in		:	in	std_logic;
		res_n			:	in std_logic;
		ena_in		:	in std_logic;
		load_in		:	in std_logic;
		up_down_in	: 	in std_logic;
		par_in		: 	in unsigned(5 downto 0);
		count_out 	:	out unsigned(5 downto 0)
	);
	end component;
	
	signal clk_sig			:	std_logic;
	signal res_sig			:	std_logic;
	signal ena_sig			:	std_logic;
	signal load_sig		:	std_logic;
	signal up_down_sig	: 	std_logic;
	signal par_sig			: 	unsigned(5 downto 0);
	signal count_sig 		:	unsigned(5 downto 0);
	signal clk_period		: 	time := 1us;
	
	begin
		uut: counter6bit
		port map
		(
			clk_in  => clk_sig,
			res_n	=>	res_sig,
			ena_in	=>	ena_sig,
			load_in => load_sig,
			up_down_in => up_down_sig,
			par_in => par_sig,
			count_out => count_sig
			
		);
		
		clk_proc: process
		begin 
			clk_sig <= '1';
			wait for 0.5 * clk_period;
			clk_sig <= '0';
			wait for 0.5 * clk_period;
		end process;
		
		res_proc: process
		begin
			res_sig <= '1';
			wait for 1.25 * clk_period;
			res_sig <= '0';
			wait for 1 * clk_period;
			res_sig <= '1';
			wait; --waiting forever
		end process;
		
		ena_proc: process
			begin
				ena_sig <= '0';
				wait for 1.25 * clk_period;
				ena_sig <= '1';
				wait;
			end process;
		
		load_proc: process
			begin
				load_sig <= '0';
				wait for 5 * clk_period;
				load_sig <= '1';
				wait for 1 * clk_period;
				load_sig <= '0';
				wait;
			end process;
		
		up_down_proc: process
			begin
				up_down_sig <= '0';
				wait for 7 * clk_period;
				up_down_sig <= '1';
			end process;
			
			
end architecture;