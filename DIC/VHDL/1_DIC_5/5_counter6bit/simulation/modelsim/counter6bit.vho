-- Copyright (C) 2016  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Intel and sold by Intel or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.1.0 Build 196 10/24/2016 SJ Lite Edition"

-- DATE "10/17/2018 13:18:47"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_ASDO_DATA1~	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_FLASH_nCE_nCSO~	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DCLK~	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_DATA0~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCEO~	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: 8mA


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~padout\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~padout\ : std_logic;
SIGNAL \~ALTERA_DATA0~~padout\ : std_logic;
SIGNAL \~ALTERA_ASDO_DATA1~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_FLASH_nCE_nCSO~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_DATA0~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	counter IS
    PORT (
	clk_in : IN std_logic;
	res_n : IN std_logic;
	ena_in : IN std_logic;
	load_in : IN std_logic;
	up_down_in : IN std_logic;
	par_in : IN std_logic_vector(5 DOWNTO 0);
	count_out : BUFFER std_logic_vector(5 DOWNTO 0)
	);
END counter;

-- Design Ports Information
-- count_out[0]	=>  Location: PIN_J13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- count_out[1]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- count_out[2]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- count_out[3]	=>  Location: PIN_L15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- count_out[4]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- count_out[5]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk_in	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- par_in[0]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- res_n	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- load_in	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ena_in	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- up_down_in	=>  Location: PIN_L13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- par_in[1]	=>  Location: PIN_L14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- par_in[2]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- par_in[3]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- par_in[4]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- par_in[5]	=>  Location: PIN_N15,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF counter IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk_in : std_logic;
SIGNAL ww_res_n : std_logic;
SIGNAL ww_ena_in : std_logic;
SIGNAL ww_load_in : std_logic;
SIGNAL ww_up_down_in : std_logic;
SIGNAL ww_par_in : std_logic_vector(5 DOWNTO 0);
SIGNAL ww_count_out : std_logic_vector(5 DOWNTO 0);
SIGNAL \res_n~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk_in~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \count_out[0]~output_o\ : std_logic;
SIGNAL \count_out[1]~output_o\ : std_logic;
SIGNAL \count_out[2]~output_o\ : std_logic;
SIGNAL \count_out[3]~output_o\ : std_logic;
SIGNAL \count_out[4]~output_o\ : std_logic;
SIGNAL \count_out[5]~output_o\ : std_logic;
SIGNAL \clk_in~input_o\ : std_logic;
SIGNAL \clk_in~inputclkctrl_outclk\ : std_logic;
SIGNAL \count_sig[0]~6_combout\ : std_logic;
SIGNAL \par_in[0]~input_o\ : std_logic;
SIGNAL \res_n~input_o\ : std_logic;
SIGNAL \res_n~inputclkctrl_outclk\ : std_logic;
SIGNAL \load_in~input_o\ : std_logic;
SIGNAL \ena_in~input_o\ : std_logic;
SIGNAL \up_down_in~input_o\ : std_logic;
SIGNAL \count_sig[0]~7\ : std_logic;
SIGNAL \count_sig[1]~8_combout\ : std_logic;
SIGNAL \par_in[1]~input_o\ : std_logic;
SIGNAL \count_sig[1]~9\ : std_logic;
SIGNAL \count_sig[2]~10_combout\ : std_logic;
SIGNAL \par_in[2]~input_o\ : std_logic;
SIGNAL \count_sig[2]~11\ : std_logic;
SIGNAL \count_sig[3]~12_combout\ : std_logic;
SIGNAL \par_in[3]~input_o\ : std_logic;
SIGNAL \count_sig[3]~13\ : std_logic;
SIGNAL \count_sig[4]~14_combout\ : std_logic;
SIGNAL \par_in[4]~input_o\ : std_logic;
SIGNAL \count_sig[4]~15\ : std_logic;
SIGNAL \count_sig[5]~16_combout\ : std_logic;
SIGNAL \par_in[5]~input_o\ : std_logic;
SIGNAL count_sig : std_logic_vector(5 DOWNTO 0);

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_clk_in <= clk_in;
ww_res_n <= res_n;
ww_ena_in <= ena_in;
ww_load_in <= load_in;
ww_up_down_in <= up_down_in;
ww_par_in <= par_in;
count_out <= ww_count_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\res_n~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \res_n~input_o\);

\clk_in~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk_in~input_o\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: IOOBUF_X53_Y16_N9
\count_out[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => count_sig(0),
	devoe => ww_devoe,
	o => \count_out[0]~output_o\);

-- Location: IOOBUF_X53_Y15_N9
\count_out[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => count_sig(1),
	devoe => ww_devoe,
	o => \count_out[1]~output_o\);

-- Location: IOOBUF_X53_Y8_N23
\count_out[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => count_sig(2),
	devoe => ww_devoe,
	o => \count_out[2]~output_o\);

-- Location: IOOBUF_X53_Y11_N2
\count_out[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => count_sig(3),
	devoe => ww_devoe,
	o => \count_out[3]~output_o\);

-- Location: IOOBUF_X53_Y11_N9
\count_out[4]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => count_sig(4),
	devoe => ww_devoe,
	o => \count_out[4]~output_o\);

-- Location: IOOBUF_X53_Y14_N9
\count_out[5]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => count_sig(5),
	devoe => ww_devoe,
	o => \count_out[5]~output_o\);

-- Location: IOIBUF_X0_Y16_N8
\clk_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk_in,
	o => \clk_in~input_o\);

-- Location: CLKCTRL_G2
\clk_in~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk_in~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk_in~inputclkctrl_outclk\);

-- Location: LCCOMB_X52_Y13_N8
\count_sig[0]~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \count_sig[0]~6_combout\ = count_sig(0) $ (VCC)
-- \count_sig[0]~7\ = CARRY(count_sig(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => count_sig(0),
	datad => VCC,
	combout => \count_sig[0]~6_combout\,
	cout => \count_sig[0]~7\);

-- Location: IOIBUF_X53_Y17_N8
\par_in[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_par_in(0),
	o => \par_in[0]~input_o\);

-- Location: IOIBUF_X0_Y16_N15
\res_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_res_n,
	o => \res_n~input_o\);

-- Location: CLKCTRL_G4
\res_n~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \res_n~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \res_n~inputclkctrl_outclk\);

-- Location: IOIBUF_X53_Y17_N1
\load_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_load_in,
	o => \load_in~input_o\);

-- Location: IOIBUF_X53_Y13_N8
\ena_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_ena_in,
	o => \ena_in~input_o\);

-- Location: FF_X52_Y13_N9
\count_sig[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \count_sig[0]~6_combout\,
	asdata => \par_in[0]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \load_in~input_o\,
	ena => \ena_in~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count_sig(0));

-- Location: IOIBUF_X53_Y10_N15
\up_down_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_up_down_in,
	o => \up_down_in~input_o\);

-- Location: LCCOMB_X52_Y13_N10
\count_sig[1]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \count_sig[1]~8_combout\ = (count_sig(1) & ((\up_down_in~input_o\ & (!\count_sig[0]~7\)) # (!\up_down_in~input_o\ & (\count_sig[0]~7\ & VCC)))) # (!count_sig(1) & ((\up_down_in~input_o\ & ((\count_sig[0]~7\) # (GND))) # (!\up_down_in~input_o\ & 
-- (!\count_sig[0]~7\))))
-- \count_sig[1]~9\ = CARRY((count_sig(1) & (\up_down_in~input_o\ & !\count_sig[0]~7\)) # (!count_sig(1) & ((\up_down_in~input_o\) # (!\count_sig[0]~7\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count_sig(1),
	datab => \up_down_in~input_o\,
	datad => VCC,
	cin => \count_sig[0]~7\,
	combout => \count_sig[1]~8_combout\,
	cout => \count_sig[1]~9\);

-- Location: IOIBUF_X53_Y9_N8
\par_in[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_par_in(1),
	o => \par_in[1]~input_o\);

-- Location: FF_X52_Y13_N11
\count_sig[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \count_sig[1]~8_combout\,
	asdata => \par_in[1]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \load_in~input_o\,
	ena => \ena_in~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count_sig(1));

-- Location: LCCOMB_X52_Y13_N12
\count_sig[2]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \count_sig[2]~10_combout\ = ((count_sig(2) $ (\up_down_in~input_o\ $ (\count_sig[1]~9\)))) # (GND)
-- \count_sig[2]~11\ = CARRY((count_sig(2) & ((!\count_sig[1]~9\) # (!\up_down_in~input_o\))) # (!count_sig(2) & (!\up_down_in~input_o\ & !\count_sig[1]~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count_sig(2),
	datab => \up_down_in~input_o\,
	datad => VCC,
	cin => \count_sig[1]~9\,
	combout => \count_sig[2]~10_combout\,
	cout => \count_sig[2]~11\);

-- Location: IOIBUF_X53_Y9_N22
\par_in[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_par_in(2),
	o => \par_in[2]~input_o\);

-- Location: FF_X52_Y13_N13
\count_sig[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \count_sig[2]~10_combout\,
	asdata => \par_in[2]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \load_in~input_o\,
	ena => \ena_in~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count_sig(2));

-- Location: LCCOMB_X52_Y13_N14
\count_sig[3]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \count_sig[3]~12_combout\ = (\up_down_in~input_o\ & ((count_sig(3) & (!\count_sig[2]~11\)) # (!count_sig(3) & ((\count_sig[2]~11\) # (GND))))) # (!\up_down_in~input_o\ & ((count_sig(3) & (\count_sig[2]~11\ & VCC)) # (!count_sig(3) & 
-- (!\count_sig[2]~11\))))
-- \count_sig[3]~13\ = CARRY((\up_down_in~input_o\ & ((!\count_sig[2]~11\) # (!count_sig(3)))) # (!\up_down_in~input_o\ & (!count_sig(3) & !\count_sig[2]~11\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \up_down_in~input_o\,
	datab => count_sig(3),
	datad => VCC,
	cin => \count_sig[2]~11\,
	combout => \count_sig[3]~12_combout\,
	cout => \count_sig[3]~13\);

-- Location: IOIBUF_X53_Y12_N1
\par_in[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_par_in(3),
	o => \par_in[3]~input_o\);

-- Location: FF_X52_Y13_N15
\count_sig[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \count_sig[3]~12_combout\,
	asdata => \par_in[3]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \load_in~input_o\,
	ena => \ena_in~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count_sig(3));

-- Location: LCCOMB_X52_Y13_N16
\count_sig[4]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \count_sig[4]~14_combout\ = ((\up_down_in~input_o\ $ (count_sig(4) $ (\count_sig[3]~13\)))) # (GND)
-- \count_sig[4]~15\ = CARRY((\up_down_in~input_o\ & (count_sig(4) & !\count_sig[3]~13\)) # (!\up_down_in~input_o\ & ((count_sig(4)) # (!\count_sig[3]~13\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \up_down_in~input_o\,
	datab => count_sig(4),
	datad => VCC,
	cin => \count_sig[3]~13\,
	combout => \count_sig[4]~14_combout\,
	cout => \count_sig[4]~15\);

-- Location: IOIBUF_X53_Y14_N1
\par_in[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_par_in(4),
	o => \par_in[4]~input_o\);

-- Location: FF_X52_Y13_N17
\count_sig[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \count_sig[4]~14_combout\,
	asdata => \par_in[4]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \load_in~input_o\,
	ena => \ena_in~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count_sig(4));

-- Location: LCCOMB_X52_Y13_N18
\count_sig[5]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \count_sig[5]~16_combout\ = \up_down_in~input_o\ $ (\count_sig[4]~15\ $ (!count_sig(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011000011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \up_down_in~input_o\,
	datad => count_sig(5),
	cin => \count_sig[4]~15\,
	combout => \count_sig[5]~16_combout\);

-- Location: IOIBUF_X53_Y9_N15
\par_in[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_par_in(5),
	o => \par_in[5]~input_o\);

-- Location: FF_X52_Y13_N19
\count_sig[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \count_sig[5]~16_combout\,
	asdata => \par_in[5]~input_o\,
	clrn => \res_n~inputclkctrl_outclk\,
	sload => \load_in~input_o\,
	ena => \ena_in~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count_sig(5));

ww_count_out(0) <= \count_out[0]~output_o\;

ww_count_out(1) <= \count_out[1]~output_o\;

ww_count_out(2) <= \count_out[2]~output_o\;

ww_count_out(3) <= \count_out[3]~output_o\;

ww_count_out(4) <= \count_out[4]~output_o\;

ww_count_out(5) <= \count_out[5]~output_o\;
END structure;


