onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /counter/clk_in
add wave -noupdate /counter/res_n
add wave -noupdate /counter/ena_in
add wave -noupdate /counter/load_in
add wave -noupdate /counter/up_down_in
add wave -noupdate /counter/par_in
add wave -noupdate -radix unsigned -radixshowbase 0 /counter/count_out
add wave -noupdate /counter/count_sig
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {35267464 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 153
configure wave -valuecolwidth 85
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {100 ns} {42100 ns}
