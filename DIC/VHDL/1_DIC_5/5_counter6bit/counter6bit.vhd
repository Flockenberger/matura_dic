-- counter
-- Author: Florian Wagner
-- Last Modified: 10.10.2018


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity counter is

	port
	(
		clk_in		:	in	std_logic;
		res_n		:	in std_logic;
		ena_in		:	in std_logic;
		load_in		:	in std_logic;
		up_down_in	: 	in std_logic;
		par_in		: 	in unsigned(5 downto 0);
		count_out 	:	out unsigned(5 downto 0)
	);
	
end counter;


architecture rtl of counter is

	signal count_sig	:	unsigned(5 downto 0);
	
	begin 
		
		count_p: process(clk_in, res_n)
		
		begin 
		
			if(res_n = '0') then 
				count_sig <= "000000";
			elsif(clk_in 'event and clk_in = '1') then
				
				if(ena_in = '1') then
			
					if(load_in = '1') then
						count_sig <= par_in;
					else
						if(up_down_in = '1') then
							count_sig <= count_sig + 1;
						else
							count_sig <= count_sig - 1;
						
							
						end if;
					end if;
				end if;
			end if;
		end process;
		
		count_out <= count_sig;
		
end rtl;
