-- FSM_ATM
-- Author: Florian Wagner
-- Last Modified: 27.04.2019


library ieee;
use ieee.std_logic_1164.all;

entity FSM_ATM is 
	port
	(
		clk			:	in 	std_logic;
		card_i		: 	in		std_logic;
		cancel_i		: 	in		std_logic;
		service_i	:	in		std_logic;
		fixed_i		:	in		std_logic;
		led_o			:	out	std_logic;
		res_n			:	in		std_logic
	);
	
end entity;



architecture rtl of FSM_ATM is
	
	
	type state_def is (idle, stactive, oos); 
	  
	attribute syn_enum_encoding			: string;
	attribute syn_enum_encoding			of state_def	:	type is "onehot";

	signal state, nstate				: state_def;
	signal done_i						: std_logic;
	
	begin
	
		res_p : process(res_n, clk)
		begin
			if(res_n = '0') then
				state <= idle;
				
			elsif rising_edge(clk) then
				state <= nstate;
				
			end if;
		end process;
		
		state_p : process(state)
		begin
		
		case state is 
		
			when idle =>
				led_o <= '1';
				done_i <= '0';
				
				if(card_i = '1') then
					nstate <= stactive;
					
				elsif(service_i = '1')  then
					nstate <= oos;
					
				else 
					nstate <= idle;
					
				end if;
			
			when stactive =>
				led_o <= '1';
				
				if(cancel_i = '1') then
					nstate <= idle;
					
				elsif(service_i = '1') then
					nstate <= oos;
					
				else
					done_i <= '1';
					nstate <= idle;
				end if;
				
			when oos =>
				led_o <= '0';
				done_i <= '0';
				
				if(fixed_i = '1') then
					nstate <= idle;
					
				end if;
				
			when others =>
				nstate <= idle;
				
		end case;
		end process;
		
	end;