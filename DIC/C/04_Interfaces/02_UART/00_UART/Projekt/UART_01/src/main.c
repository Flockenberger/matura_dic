/*
 * Author: Florian Wagner
 * Programm: main.c - Anwendung des UART
 */

#include "uart.h"
#include <stdlib.h>

#define RANDOM_MAX 0x1A
#define INIT_RANDOM(D) srand(D)
#define CAP_LETTER ((uint8_t)rand() % RANDOM_MAX) + 'A'
#define NOR_LETTER ((uint8_t)rand() % RANDOM_MAX) + 'a'


int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	/* Insert application code here, after the board has been initialized. */
	uart0_init(uart_baud(9600));
	int p = 0;
	int xCount = 0;
	int XCount = 0;
	for(;;)
	{
		uint8_t data = uart0_rec();
		if(p == 0)
		{
			INIT_RANDOM(data);
			p = 1;
		}
		else if(data == 'X'){
			XCount++;
		}
		else if(data == 'x'){
			xCount++;
		}
		else if(data =='_'){
			uart0_send_string("x count : ");
			uart0_send(xCount + '0');
			uart0_send('\n');
			uart0_send_string("X count: ");
			uart0_send(XCount + '0');
			uart0_send('\n');
		}
		else if(data == 'g')
		{
			uart0_send(CAP_LETTER);
			uart0_send(NOR_LETTER);
			uart0_send(CAP_LETTER);
			uart0_send(NOR_LETTER);
			uart0_send(CAP_LETTER);
			uart0_send('\n');
		}
		
		//uart0_flush();
		//_delay_ms(1000);
	}
	
}
