/*
* uart.h
*
* Created: 15.05.2018 11:52:01
* Author: Florian Wagner
* Dieses File enthät alles was man für das UART braucht... sowohl für UART0 als auch UART1 (im Prinzip sind nur die Zahlen andere).
* Fast 1 zu 1 Kopie aus dem Datenblatt!
*/


#ifndef UART_H_
#define UART_H_
#define F_CPU 16e6
#include <util/delay.h>
#include <asf.h>

void uart0_init(uint16_t);
void uart0_send(uint8_t);
uint8_t uart0_rec(void);
void uart0_flush(void);

void uart1_init(uint16_t);
void uart1_send(uint8_t);
uint8_t uart1_rec(void);
void uart1_flush(void);
uint16_t uart_baud(uint16_t);


void uart1_init( uint16_t baud )
{
	/* Set baud rate */
	UBRR1H = (uint8_t) baud >> 8;
	UBRR1L = (uint8_t) baud;
	
	/* Enable receiver and transmitter */
	UCSR1B |= (1<<RXEN1)|(1<<TXEN1);
	
	/* Set frame format: 8data, 2stop bit, parity:even*/
	UCSR1C |= (2<<UPM10) |(1<<USBS1);
	UCSR1C |= (3<<UCSZ10);
}

void uart1_send( uint8_t data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR1A & (1<<UDRE1)) )
	;
	/* Put data into buffer, sends the data */
	UDR1 = data;
}

void uart1_send_string(const char *string)
{
	while(*string != '\0'){
		uart1_send(*string);
		string++;
	}
}
uint8_t uart1_rec()
{
	/* Wait for data to be received */
	while ( !(UCSR1A & (1<<RXC1)) )
	;
	/* Get and return received data from buffer */
	return UDR1;
}

void uart1_flush()
{
	uint8_t dummy;
	while ( UCSR1A & (1<<RXC1) ) dummy = UDR1;
}


uint16_t uart_baud(uint16_t baud)
{
	return ((F_CPU / 16 / baud) - 1);
}


void uart0_init( uint16_t baud )
{
	/* Set baud rate */
	UBRR0H = (uint8_t) baud >> 8;
	UBRR0L = (uint8_t) baud;
	
	/* Enable receiver and transmitter */
	UCSR0B |= (1<<RXEN0)|(1<<TXEN0);
	
	/* Set frame format: 8data, 2stop bit, parity:even*/
	UCSR0C |= (2<<UPM00) |(1<<USBS0);
	UCSR0C |= (3<<UCSZ00);
}

void uart0_send( uint8_t data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) )
	;
	/* Put data into buffer, sends the data */
	UDR0 = data;
}

void uart0_send_string(const char *string)
{
	while(*string != '\0'){
		uart0_send(*string);
		string++;
	}	
}
uint8_t uart0_rec()
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	/* Get and return received data from buffer */
	return UDR0;
}

void uart0_flush()
{
	uint8_t dummy;
	while ( UCSR0A & (1<<RXC0) ) dummy = UDR0;
}

#endif /* UART_H_ */