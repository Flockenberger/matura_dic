/*
 * Author: Florian Wagner
 * Programm: I2C:Gemini
 * Kombination aus I2C und SPI_HCT595 Programm.
 * Wirklich fast Copy-Paste....
 */

#define F_CPU 16e6


#include <util/delay.h>
#include <asf.h>

#define w84INT while(!(TWCR & (1<<TWINT)));
#define i2c_start TWCR = ((1<<TWINT) | (1<<TWSTA) | (1<<TWEN));
#define i2c_rec_ak TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
#define i2c_rec_nak TWCR = (1<<TWINT) | (1<<TWEN);
#define i2c_stop  TWCR = ((1<<TWINT) | (1<<TWSTO) | (1<<TWEN));



#define OE_PIN PORTB3
#define STCP_PIN PORTB2
#define MR_PIN PORTB1
#define CS_PIN PORTB4

#define csHIGH PORTB|=(1<<CS_PIN)
#define csLOW PORTB&=~(1<<CS_PIN)

#define ADDR_WR 0x90
#define ADDR_RD 0x91

#define BITRATE 42

void i2c_init(void);
uint16_t i2c_rec();
void i2c_send_addr(uint8_t addr);
void i2c_send(uint8_t addr, uint8_t reg, uint16_t data);

void
i2c_send_addr(uint8_t addr)
{
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN);
	w84INT;
}

uint16_t
i2c_rec()
{
	uint16_t data = 0;
	i2c_start;
	w84INT;
	i2c_send_addr(ADDR_RD);
	i2c_rec_ak;
	w84INT;
	data |= (TWDR << 8);
	i2c_rec_nak;
	w84INT;
	data |= TWDR;
	i2c_stop;
	return data;
}

void
i2c_send_data(uint8_t data)
{
	
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);
	w84INT;
}

void
i2c_send(uint8_t addr, uint8_t reg,  uint16_t data)
{
	
	i2c_start;
	w84INT;
	i2c_send_addr(addr);
	i2c_send_data(reg);
	i2c_send_data(data >> 8);
	i2c_send_data(data & 0xFF);
	i2c_stop;
	
}

void
i2c_init(void)
{
	TWBR = BITRATE;
	TWSR = 0;
}



void configSPI();
uint8_t sendSPI(uint8_t);


void configSPI(){
	SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR1) | (1<<SPR0);
}

uint8_t inline sendSPI(uint8_t data){
	SPDR = data;
	while(!(SPSR & (1<<SPIF)));
	SPSR|= (1<<SPIF);
	return SPDR; //returned byte by the slave
}

void initHCT595(){
	
	PORTB &= ~(1<<OE_PIN) & ~(1<<STCP_PIN) & ~(1<<MR_PIN);
	PORTB |= (1<<STCP_PIN);
	_delay_us(10);
	PORTB &= ~(1<<STCP_PIN);
	PORTB |= (1<<MR_PIN) | (1<<OE_PIN);
}

int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	
	//DDRB = 0xFF;
	PORTC = 0x00;
	PORTC = (1<<PORTC0) | (1<<PORTC1);
	
	DDRB |= (1<<CS_PIN) | (1<<MOSI_BIT) | (1<<SCK_BIT) | (1<<OE_PIN);
	DDRB &= ~(1<<MISO_BIT);
	DDRB |= (1<<STCP_PIN);
	
	//PORTB = 0xFF;
	//for(;;);
	board_init();
	i2c_init();
	
	initHCT595();
	configSPI();
	
	
	//PORTB &= ~(1<<OE_PIN);
	//i2c_send(ADDR_WR, 0x03, 0x140);
	
	uint16_t data= 0xAAAA;
	csLOW;
	for(;;)
	{
		
		data = i2c_rec();
		//PORTB = (data >> 8);
		//_delay_ms(100);
		
		sendSPI(data >> 8);
		sendSPI(data);
		
		_delay_us(60);
		PORTB |= (1<<STCP_PIN);
		_delay_us(10);
		PORTB &= ~(1<<STCP_PIN);
		_delay_ms(200);
		//csHIGH;

		//PORTB ^= (1<<OE_PIN);
		
	}
}
