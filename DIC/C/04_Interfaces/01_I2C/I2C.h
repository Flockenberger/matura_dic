/*
 * i2c.h
 *
 * Created: 17.04.2018 12:14:07
 * Author: Florian Wagner
 * Diese Datei ist nur eine veranschaulichung dessen was wirklich benötigt wird für das I2C Interface.
 * Ist nicht viel und eigentlich fast alles aus dem Datenblatt
 */ 


#ifndef I2C_H_
#define I2C_H_


#define F_CPU 16e6


#include <util/delay.h>
#include <asf.h>

#define w84INT while(!(TWCR & (1<<TWINT)));
#define i2c_start TWCR = ((1<<TWINT) | (1<<TWSTA) | (1<<TWEN));
#define i2c_rec_ak TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
#define i2c_rec_nak TWCR = (1<<TWINT) | (1<<TWEN);
#define i2c_stop  TWCR = ((1<<TWINT) | (1<<TWSTO) | (1<<TWEN));

#define ADDR_WR 0x90 //diese Adress müsste angepasst werden
#define ADDR_RD 0x91 //Diese auch

#define BITRATE 42 //42 ist immer die Antwort (nein ist sie nicht)

void i2c_init(void);
uint16_t i2c_rec();
void i2c_send_addr(uint8_t addr);
void i2c_send(uint8_t addr, uint8_t reg, uint16_t data);

void
i2c_send_addr(uint8_t addr)
{
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN);
	w84INT;
}

uint16_t
i2c_rec()
{
	uint16_t data = 0;
	i2c_start;
	w84INT;
	i2c_send_addr(ADDR_RD);
	i2c_rec_ak;
	w84INT;
	data |= (TWDR << 8);
	i2c_rec_nak;
	w84INT;
	data |= TWDR;
	i2c_stop;
	return data;
}

void
i2c_send_data(uint8_t data)
{
	
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);
	w84INT;
}

void
i2c_send(uint8_t addr, uint8_t reg,  uint16_t data)
{
	
	i2c_start;
	w84INT;
	i2c_send_addr(addr);
	i2c_send_data(reg);
	i2c_send_data(data >> 8);
	i2c_send_data(data & 0xFF);
	i2c_stop;
	
}

void
i2c_init(void)
{
	TWBR = BITRATE;
	TWSR = 0x00;
}
#endif /* I2C_H_ */