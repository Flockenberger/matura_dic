/*
 * Author: Florian Wagner
 * Programm: I2C
 */
 

#define F_CPU 16e6


#include <util/delay.h>
#include <asf.h>

//defines that make the code so much easier to write
#define w84INT while(!(TWCR & (1<<TWINT))); 
#define i2c_start TWCR = ((1<<TWINT) | (1<<TWSTA) | (1<<TWEN));
#define i2c_rec_ak TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
#define i2c_rec_nak TWCR = (1<<TWINT) | (1<<TWEN);
#define i2c_stop  TWCR = ((1<<TWINT) | (1<<TWSTO) | (1<<TWEN));

#define ADDR_WR 0x90
#define ADDR_RD 0x91
#define BITRATE 42

void i2c_init(void);
uint16_t i2c_rec();
void i2c_send_addr(uint8_t addr);
void i2c_send(uint8_t addr, uint8_t reg, uint16_t data);

void
i2c_send_addr(uint8_t addr)
{
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN);
	w84INT;
}

uint16_t
i2c_rec()
{
	uint16_t data = 0;
	i2c_start;
	w84INT;
	i2c_send_addr(ADDR_RD);
	i2c_rec_ak;
	w84INT;
	data |= (TWDR << 8);
	i2c_rec_nak;
	w84INT;
	data |= TWDR;
	i2c_stop;
	return data;
}

void 
i2c_send_data(uint8_t data)
{
	
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);
	w84INT;
}

void 
i2c_send(uint8_t addr, uint8_t reg,  uint16_t data)
{
	
	i2c_start;
	w84INT;
	i2c_send_addr(addr);
	i2c_send_data(reg);
	i2c_send_data(data >> 8);
	i2c_send_data(data & 0xFF);
	i2c_stop;
	
}

void
i2c_init(void)
{
	TWBR = BITRATE;
	TWSR = 0x00;
}

// 30� = 0x1E00


int main (void)
{

	DDRB = 0xFF;
	PORTC = 0x00;
	PORTC = (1<<PORTC0) | (1<<PORTC1);
	
	board_init();
	
	i2c_init();
	i2c_send(ADDR_WR, 0x03, 0x140);
	
	for(;;)
	{
		uint16_t data = i2c_rec();
		PORTB = (data >> 8);
		_delay_ms(100);
	}
}
