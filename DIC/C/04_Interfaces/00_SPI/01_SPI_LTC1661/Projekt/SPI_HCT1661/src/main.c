/*
 * Author: Florian Wagner
 * Programm: SPI_LTC1661
 */
 
#define F_CPU 16e6

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>

#define SPI_SPEED (1 << SPR1) | (1 << SPR0)

#define DELAY 2

#define CTRL 0x09

#define SS_AND SS_PORT &= ~(1 << SS_BIT);
#define SS_OR SS_PORT |= (1 << SS_BIT);

static uint8_t transferSPI(uint8_t data);
static uint16_t transferSPI16(uint16_t data);
static inline void initSPI(void);

/*
1001 0011 0011 0100
*/

int main() {
	initSPI();

	uint16_t data = 0;
	
	for(;;) {
		if(data >= 1024)
			data = 0;
		SS_AND;
		
		_delay_us(8);
		transferSPI(CTRL << 4 | data >> 6);
		transferSPI(data << 2);
		_delay_us(8);
		
		SS_OR;
		data++;
	}
}

static inline void initSPI(void) {
	MOSI_DDR |= (1 << MOSI_BIT);
	SS_DDR |= (1 << SS_BIT);
	SCK_DDR |= (1 << SCK_BIT);
	MISO_DDR &= ~(1 << MISO_BIT);
	SPCR |=  (1 << MSTR) | (1 << SPE);
}

static uint8_t transferSPI(uint8_t data) {
	SPDR = data;
	while (!(SPSR & (1 << SPIF)))
	;
	return SPDR;
}
