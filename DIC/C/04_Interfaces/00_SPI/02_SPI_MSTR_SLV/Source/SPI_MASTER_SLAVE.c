/*
 * Author: Florian Wagner
 * Programm: SPI_MASTER_SLAVE
 */
#define F_CPU 16e6

#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>
#include <interrupt.h>

#define SPI_SPEED (1 << SPR1) | (1 << SPR0)

#define SS_AND SS_PORT &= ~(1 << SS_BIT);
#define SS_OR SS_PORT |= (1 << SS_BIT);

#define TRUE 1
#define FALSE 0

static uint8_t transferSPI(uint8_t data);
static uint16_t transferSPI16(uint16_t data);
static void startSPI(void);
static void stopSPI(void);
static inline void initSPI(void);
static inline uint8_t doSPITransfer(uint8_t data);

#define MASTER

int main()
{
	DDRC = 0xFF;
	initSPI();
	uint8_t data = 0xAA;
	uint8_t ret = 0;
	for(;;) {
		ret = doSPITransfer(data);
		PORTC = ret;
		_delay_ms(200);
		data++;
	}
}

ISR(SPI_STC_vect)
{
	SPDR = ~SPDR;
}

static inline uint8_t
doSPITransfer(uint8_t data)
{
	uint8_t tmp = 0;
	#ifdef MASTER
	
	startSPI();
	tmp = transferSPI(data);
	stopSPI();
	
	#endif
	return tmp;
}

static void
startSPI(void)
{
	SS_AND;
	_delay_us(8);
}

static void
stopSPI(void)
{
	_delay_us(8);
	SS_OR;
}

static inline void
initSPI()
{
	
	#ifdef MASTER
		MOSI_DDR |= (1 << MOSI_BIT);
		SS_DDR |= (1 << SS_BIT);
		SCK_DDR |= (1 << SCK_BIT);
		MISO_DDR &= ~(1 << MISO_BIT);
		SPCR |=  (1 << MSTR)| (1 << SPE) | (1<<SPR0);
	//SPSR |= (1 << SPI2X);
	#else
		sei();
		SPCR |= (1<<SPIE);
		MOSI_DDR &= ~(1 << MOSI_BIT);
		SS_DDR &= ~(1 << SS_BIT);
		SCK_DDR &= ~(1 << SCK_BIT);
		MISO_DDR |= (1 << MISO_BIT);
		SPCR &= ~(1 << SPE);
	#endif
}

static uint8_t
transferSPI(uint8_t data)
{
	SPDR = data;
	while (!(SPSR & (1 << SPIF)))
	;
	return SPDR;
}
