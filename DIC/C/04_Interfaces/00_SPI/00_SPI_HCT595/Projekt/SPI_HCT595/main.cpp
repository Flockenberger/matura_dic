/*
 * SPI_HCT595.cpp
 *
 * Created: 30.01.2018 11:47:02
 * Author : Florian Wagner
 */ 

#define F_CPU 10e6

#include <avr/io.h>
#include <util/delay.h>

#define OE_PIN PORTB3
#define STCP_PIN PORTB2
#define MR_PIN PORTB1
#define CS_PIN PORTB4

#define csHIGH PORTB|=(1<<CS_PIN)
#define csLOW PORTB&=~(1<<CS_PIN)


void configSPI();
uint8_t sendSPI(uint8_t);


void configSPI(){
	/* Enable SPI, Master, set clock rate*/
	SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR1) | (1<<SPR0);
}

uint8_t inline sendSPI(uint8_t data){
	
	/* Start transmission */
	SPDR = data;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF)));
	SPSR|= (1<<SPIF);
	return SPDR; //returned byte by the slave
}

void initHCT595(){
	
	PORTB &= ~(1<<OE_PIN) & ~(1<<STCP_PIN) & ~(1<<MR_PIN);
	PORTB |= (1<<STCP_PIN);
	_delay_us(10);
	PORTB &= ~(1<<STCP_PIN);
	PORTB |= (1<<MR_PIN) | (1<<OE_PIN);
}

int main(void)
{
	
	DDRB |= (1<<CS_PIN) | (1<<MOSI_BIT) | (1<<SCK_BIT) | (1<<OE_PIN);
	DDRB &= ~(1<<MISO_BIT); 
	
	initHCT595();
	configSPI();
	PORTB &= ~(1<<OE_PIN);
	
    for(;;)
    {	
		
		csLOW; //chip select -> low 
		
		sendSPI(0xAA); //send 0xAA to slave
		_delay_us(60);
		
		PORTB |= (1<<STCP_PIN);
		_delay_us(10);
		
		PORTB &= ~(1<<STCP_PIN);
		_delay_ms(200);
		
		sendSPI(0x55); //send 0x55 to slave
		
		PORTB |= (1<<STCP_PIN);
		_delay_us(10);
		
		PORTB &= ~(1<<STCP_PIN);
		_delay_ms(200);
		
		csHIGH; //chip select -> high 
    }
}

