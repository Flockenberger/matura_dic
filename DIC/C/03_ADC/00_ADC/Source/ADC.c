/*
 * Author: Florian Wagner
 * Programm: ADC
 */
 
#include <asf.h>

volatile int bL = 0;
volatile int bH = 0;

void init_ADC(void);

void init_ADC(){
	
	//ADMUX – ADC Multiplexer Selection Register
	ADMUX |= (1<<REFS0); //Bit 7:6 – REFS1:0: Reference Selection Bits -> External Reference Voltage!
	ADMUX |= (1<<ADLAR); //Bit 5 – ADLAR: ADC Left Adjust Result
	ADMUX |= (1<<MUX0); //Bits 4:0 – MUX4:0: Analog Channel and Gain Selection Bits
	
	/*	
	*	ADC prescaler selections
	*	ADPS2 ADPS1 ADPS0 	Division factor
	*	0 	  0 	0 		2
	*	0 	  0 	1 		2
	*	0 	  1 	0 		4
	*	0 	  1 	1 		8
	*	1 	  0 	0 		16
	*	1 	  0 	1 		32
	*	1 	  1 	0 		64
	*	1 	  1 	1 		128
	*/	
	
	// ADCSRA – ADC Control and Status Register A
	ADCSRA |=(1<<ADPS2);
	ADCSRA |=(1<<ADPS1);
	ADCSRA |= (1<<ADEN); // Bit 7 – ADEN: ADC Enable
}

int main (void)
{
	init_ADC();
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	DDRB = 0xFF; // set the register b as output; 1->output, 0->input!
	
	for(;;){
		ADCSRA |= (1<<ADSC);
		while(ADCSRA & (1<<ADSC));
		bL = ADCL;
		bH = ADCH;
		PORTB = bH;
	}
}
