/*
 * Author: Florian Wagner
 * Programm: Interrupt (Lauflicht)
 * Dieses Programm ist eine erweiterung des Lauflichtes aus ../../../00_Einfache_Programme/00_Lauflicht
 */
 
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>
#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt

//#define _debug

//global variables
char val = 26; //start val 00 |10 0110
char tmp; //temp variable for bit shifting
char _FTB = 0xc0; // mask for the first two bit
char _LTB = 0x3e; //mask for the last two bit
char _SB = 6;
int isInterrupt = 0;

ISR(INT0_vect){
	
	EIMSK &= ~(1<<INT0); //External Interrupt Mask Register -> clear mask of interrupt 0
	
	//Code from 00_Lauflicht
	PORTC = val; // set the new value to port c
	val = val << 1; //shift everything one left
	tmp = val & _FTB; // get the first two bit
	tmp = tmp >> _SB; // shift the first two bit to the end
	val &= _LTB; // remove the first two bits
	val |= tmp; 
	
	_delay_ms(300);
	
	EIFR = (1<<INT0); //External Interrupt Flag Register -> set interrupt flag 0
	EIMSK |= (1<<INT0); //write mask of interrupt 0 to 1
}

int main (void)
{	
	sei(); //enable global interrupt

/*	Interrupt Sense Control for EICRA
*	ISCn1 ISCn0 Description
*	 0 		0 	The low level of INTn generates an interrupt request
*	 0 		1 	Any edge of INTn generates asynchronously an interrupt request
*	 1 		0 	The falling edge of INTn generates asynchronously an interrupt request
*	 1 		1 	The rising edge of INTn generates asynchronously an interrupt request
*/	
	EICRA |= (1 << ISC01);
	//EICRA = (1<<ISC00); //not needed -> 0 by default
	
	//enable Interrupt 0
	EIMSK |= (1<<INT0); //"When an INT2:0 bit is written to one and the I-bit in the Status Register (SREG) is set (one), the corresponding external pin interrupt is enabled."
	EIFR = (1<<INT0); //clear interrupt flag -> "Alternatively, the flag can be cleared by writing a logical one to it"
	
	DDRC = 0xFF; // set the register c as output
	DDRD &= ~(1 << 2);
	PORTD |= (1 << 2);
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	//main loop
	while(1){	
		#ifndef _debug
			_delay_ms(1000); // 1 second delay
		#endif
		
	}
}
