/*
 * Author: Florian Wagner
 * Programm: Timer_PWM
 */
 
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>

#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt -> timer essentially uses interrupts

void initTimer(void);


void initTimer(void){
	
	//http://prntscr.com/ncqks8 Screenshot of the Waveform Generation Bits (WGM Bits) (Die Tabelle war zu groß xD)
	
	//TCCR0A: Timer/Counter Control Register A
	TCCR0A |= (1 << WGM01) | (1 << WGM00);
	
	//Bits 7:6 – COM0A1:0: Compare Match Output A Mode
	TCCR0A |= (1 << COM0A1); // COM
	
	TCCR0B |= (1 << CS01); //Prescaler, by now you should know...
	
	OCR0A = 0;
}

int main (void)
{
	DDRB |= (1 << PORTB3);
	
	sei(); //enable global interrupt
	
	initTimer(); //call timer initialization function
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	//we create the pwm ourselfs 
	int mode = 0;
	for(;;){ //the same as while(true){} but in fancy
		_delay_ms(1);
		if(mode == 0) OCR0A++;
		else OCR0A--;
		if(OCR0A == 255) mode = 1;
		else if (OCR0A == 0) mode = 0;
		
	}
}
