/*
 * Author: Florian Wagner
 * Programm: Timer_PWM_PC
 */
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>

#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt -> timer essentially uses interrupts


const uint16_t pwmtable_10[64] =
{
	0, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 9, 10,
	11, 12, 13, 15, 17, 19, 21, 23, 26, 29, 32, 36, 40, 44, 49, 55,
	61, 68, 76, 85, 94, 105, 117, 131, 146, 162, 181, 202, 225, 250,
	279, 311, 346, 386, 430, 479, 534, 595, 663, 739, 824, 918, 1023
};

void initTimer(void);
void initInt(void);

volatile int count = 0;
volatile int mode = 0;
volatile int c2 = 0;

#define comp1 122
#define comp2 244
volatile int compare = comp1;

ISR(INT0_vect){
	TCCR1B |= (1 << CS10);
	compare = comp1;
}
ISR(INT1_vect){
	TCCR1B |= (1 << CS10);
	compare = comp2;
}
ISR(INT2_vect){
	TCCR1B &= ~(1 << CS10);
}


ISR(TIMER1_OVF_vect){
	if (c2++ >= compare){
		OCR1A = pwmtable_10[count];
		
		if (count >= 64) count = 0;
		if(mode == 0) count++;
		else count--;
		if(count == 63) mode = 1;
		else if (count == 0) mode = 0;
		c2 = 0; // reset counter
		
	}
}

void initTimer(void){
	
	TCCR1A |= (1 << WGM11) | (1 << COM1A1); // For PWM - Mode
	TCCR1B |=  (1 << WGM13);
	
	ICR1 = 1 << 10;
	
	TCCR1B |= (1 << CS10);
	
	TIMSK1 |= (1<< TOIE1);
	
	OCR1A = 0;
}
void initInt(){	
	
	EICRA |= (1 << ISC00) | (1 << ISC01) | (1 << ISC11) |( 1<< ISC10) | (1 << ISC21) | ( 1<< ISC20);
	
	//External Interrupt Mask Register, Enable Interrupts 0-2
	EIMSK |= (1<<INT0) | (1<<INT1) | (1<<INT2);
	
	//External Interrupt Flag Register
	EIFR = (1<<INT0) | (1<<INT1) | (1<<INT2);
}
int main (void) {
	
	DDRD |= (1 << PORTD5);
	PORTD |= (1 << PORTD2) | (1 << PORTD3);
	PORTB |= (1 << PORTB2);
	
	sei(); //enable global interrupt
	
	initTimer(); //call timer initialization function
	initInt(); //call interrupt initialization function
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	for(;;){}
	
}
