/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#define F_CPU 16000000 //we must set the clock in order to use the delay function
#include <asf.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void initTimer(void);

volatile static int timeC = 0;

ISR(TIMER0_OVF_vect){
	timeC ++;
	if(timeC >= 3906){
		PORTC ^= (1 << PORTC4);
		timeC = 0;
	}
}

void initTimer(void){
	
	TCCR0B |= (1 << CS01); // prescale to 8
	TIMSK0 |= (1<< TOIE0);
}

int main (void)
{	
	DDRC = 0xFF;
	sei();
	/* Insert system clock initialization code here (sysclk_init()). */
	initTimer();
	board_init();

	for(;;){
		_delay_ms(300);
	}
	
	/* Insert application code here, after the board has been initialized. */
}
