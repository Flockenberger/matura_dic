/*
 * Author: Florian Wagner
 * Programm: Timer_simple
 * Einfacher Timer der einen Overflow Interrupt auslöst
 */
 
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>

#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt -> timer essentially uses interrupts

void initTimer(void);

//ISR routine
ISR(TIMER0_OVF_vect){
	PORTC ^= (1 << PORTC4);
}

/*
*Initializes the timer 0 
*/
void initTimer(void){
	
/*	Clock Select bit description
*	CS02 CS01 CS00  Description
*	0 	 0 	  0 	No clock source (Timer/Counter stopped)
*	0 	 0 	  1 	clkI/O/(No prescaling)
*	0 	 1 	  0 	clkI/O/8 (From prescaler)
*	0 	 1 	  1 	clkI/O/64 (From prescaler)
*	1 	 0 	  0 	clkI/O/256 (From prescaler)
*/	
	//TCCRnB Timer/Counter Control Register B
	TCCR0B |= (1 << CS01); // prescale to 8
	
	//TIMSKn: Timer/Counter Interrupt Mask Register 
	//Bit 0 – TOIE0: Timer/Counter0 Overflow Interrupt Enable
	TIMSK0 |= (1<< TOIE0);
}

int main (void)
{	
	DDRC = 0xFF; //1->output, 0->input!
	sei(); //enable global interrupt
	
	initTimer(); //call timer initialization function
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	for(;;){ //the same as while(true){} but in fancy
		_delay_ms(300);
	}
	
}
