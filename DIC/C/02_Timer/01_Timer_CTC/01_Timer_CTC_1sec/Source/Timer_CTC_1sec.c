/*
 * Author: Florian Wagner
 * Programm: Timer_CTC_1sec
 * Beinahe das Gleiche Programm wie ../../02_Timer\00_Timer_simple\
 * Diesmal im CTC Mode, damit wir die eine Sekunde erreichen können
 * Man beachte hierbei, dass wirklich nur initTimer anders ist und ein anderer interrupt Vektor verwendet wird.
 * Wir haben in diesem Programm auch Timer 1! verwendet
 */
 
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>

#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt -> timer essentially uses interrupts

void initTimer(void);

//ISR routine
ISR(TIMER1_COMPA_vect){
	PORTC ^= (1 << PORTC4);
}

void initTimer(void){
	
	//http://prntscr.com/ncq2m4 Screenshot of the Waveform Generation Bits (WGM Bits) (Die Tabelle war zu groß xD)
	
	TCCR1A |= (1 << WGM12); // CTC Mode -> Different than before! 

	/*	
	*	CSn2 CSn1 CSn0  Description
	*	0 	 0 	  0 	No clock source (Timer/Counter stopped).
	*	0 	 0 	  1 	clkI/O/1 (No prescaling)
	*	0 	 1 	  0 	clkI/O/8 (From prescaler)
	*	0 	 1 	  1 	clkI/O/64 (From prescaler)
	*	1 	 0 	  0 	clkI/O/256 (From prescaler)
	*	1 	 0 	  1 	clkI/O/1024 (From prescaler)
	*	1 	 1 	  0 	External clock source on Tn pin. Clock on falling edge.
	*	1 	 1 	  1 	External clock source on Tn pin. Clock on rising edge.
	*/
	
	TCCR1B |= (1 << CS10); // -> For prescale 1024 " 1 0 1 "
	TCCR1B |= (1 << CS12); // -> For prescale 1024 " 1 0 1 "
	
	
	//TIMSKn: Timer/Counter Interrupt Mask Register
	TIMSK1 |= (1<< OCIE1A); // CTC Mode
	
	// OCRA (CTC): fosc=(fclk)/(2*N*OCRnx) ; N -> Prescaler
	OCR1A = 7813;
}

int main (void)
{
	DDRC = 0xFF; //1->output, 0->input!
	sei(); //enable global interrupt
	
	initTimer(); //call timer initialization function
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	for(;;){ //the same as while(true){} but in fancy
		_delay_ms(300);
	}
}
