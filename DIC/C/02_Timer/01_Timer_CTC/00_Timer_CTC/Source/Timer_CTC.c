/*
 * Author: Florian Wagner
 * Programm: Timer_simple_1sec
 */
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>

#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt -> timer essentially uses interrupts

void initTimer(void);

//ISR routine
ISR(TIMER0_COMPA_vect){
	PORTC ^= (1 << PORTC4);
}

void initTimer(void){
	
	//http://prntscr.com/ncq2m4 Screenshot of the Waveform Generation Bits (WGM Bits) (Die Tabelle war zu groß xD)
	
	TCCR0A |= (1 << WGM01); // CTC Mode -> Different than before! 

	/*	
	*	CSn2 CSn1 CSn0  Description
	*	0 	 0 	  0 	No clock source (Timer/Counter stopped).
	*	0 	 0 	  1 	clkI/O/1 (No prescaling)
	*	0 	 1 	  0 	clkI/O/8 (From prescaler)
	*	0 	 1 	  1 	clkI/O/64 (From prescaler)
	*	1 	 0 	  0 	clkI/O/256 (From prescaler)
	*	1 	 0 	  1 	clkI/O/1024 (From prescaler)
	*	1 	 1 	  0 	External clock source on Tn pin. Clock on falling edge.
	*	1 	 1 	  1 	External clock source on Tn pin. Clock on rising edge.
	*/
	TCCR0B |= (1 << CS00); // -> For prescale 1024 " 1 0 1 "
	TCCR0B |= (1 << CS02); // -> For prescale 1024 " 1 0 1 "
	
	//TIMSKn:Timer/Counter Interrupt Mask Register
	//""When the OCIE0A bit is written to one, and the I-bit in the Status Register is set, the Timer/Counter0 Compare Match A interrupt is enabled."" 
	TIMSK0 |= (1<< OCIE0A); // CTC Mode
	
	OCR0A = 164;
}

int main (void)
{
	DDDRC = 0xFF; //1->output, 0->input!
	sei(); //enable global interrupt
	
	initTimer(); //call timer initialization function
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	for(;;){ //the same as while(true){} but in fancy
		_delay_ms(300);
	}
}
