/*
 * Author: Florian Wagner
 * Programm: Timer_PWM_Tabelle
 */
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>

#include <util/delay.h> //for _delay_ms
#include <avr/interrupt.h> //imports for interrupt -> timer essentially uses interrupts

void initTimer(void);


const uint16_t pwmtable_8D[32] =
{
	0, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 8, 10, 11, 13, 16, 19, 23,
	27, 32, 38, 45, 54, 64, 76, 91, 108, 128, 152, 181, 215, 255
};

volatile int count = 0;
volatile int mode = 0;
volatile int c2 = 0;

void initTimer(void){
	
	//TCCRnA: Timer/Counter Control Register A
	TCCR0A |= (1 << WGM01) | (1 << WGM00); // For PWM - Mode
	TCCR0A |= (1 << COM0A1); // COM
	
	TCCR0B |= (1 << CS01);
	
	TIMSK0 |= (1<< TOIE0);
	
	OCR0A = 0;
}
ISR(TIMER0_OVF_vect){
	if (c2++ >= 256){
		OCR0A = pwmtable_8D[count]; //set OCRA to a new value of the lookup table 
		
		//if (count >= 32) count = 0;
		if(mode == 0) count++;
		else count--;
		if(count == 31) mode = 1;
		else if (count == 0) mode = 0;
		
		c2 = 0; // reset counter
	}
}
int main (void) {
	DDRB |= (1 << PORTB3);
	
	sei(); //enable global interrupt
	
	initTimer(); //call timer initialization function
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	for(;;){}
	

}
