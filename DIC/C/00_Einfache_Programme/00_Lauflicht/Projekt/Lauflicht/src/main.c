/*
 * Author: Florian Wagner
 * Programm: Lauflicht
 */
 
#define F_CPU 16000000 //we must set the clock in order to use the delay function

#include <asf.h>
#include <avr/io.h>
#include <util/delay.h> // for _delay_ms function

//#define _debug

int main (void)
{

	DDRC = 0xFF; // set the register c as output; 1->output, 0->input!
	char val = 26; //start val 00 |10 0110
	char tmp; //temp variable for bit shifting
	char _FTB = 0xc0; // mask for the first two bit
	char _LTB = 0x3e; //mask for the last two bit
	char _SB = 6;
	
	//die Zeile könnt ihr ignorieren, des will mein Atmel Studio 
	board_init(); // initializes the board
	
	while(1){	
		
		PORTC = val; // set the new value to port c
		val = val << 1; //shift everything one left
		tmp = val & _FTB; // get the first two bit
		tmp = tmp >> _SB; // shift the first two bit to the end
		val &= _LTB; // remove the first two bits
		val |= tmp; // 
		
		#ifndef _debug
			_delay_ms(1000); // 1 second delay
		#endif
		
	}
}
